#include <iostream>

int const antinople = 50;
double noteEleve[antinople];
double noteErnest;
double potentielNoteMin;
double potentielNoteMax;
double potentielMoyenne;
int nbEleve = 0;

using namespace std;


bool verificationNombre(double x,double y ){/*Fonction de vérification avec valeurs absolue < 0.2*/

    if(abs(x-y)<0.2)

        return true;

    else

        return false;

}

double saisirUneNote()/*Fonction qui permet de saisir une seul note entre 0 et 20*/
{
    double x;
    do{
        cin >> x;
    } while(x < 0 && x>20);
    return x;
}

void saisirLesNote()/*Procédure qui permet de saisir les notes de tout les éléves dans un tableau et compte le nombre d'éléve */
{

    do{
        cout << "Saisir la note de l'eleve" << nbEleve+1 << endl;
        noteEleve[nbEleve] = saisirUneNote();
        nbEleve++;
    }while(nbEleve-1 != 50 && noteEleve[nbEleve-1] != -1);
    nbEleve--;
}

void saisirNote()/*Procédure de saisie des notes "selon Ernest"*/
{
    cout << "Saisir la note d'Ernest :" << endl;
    noteErnest = saisirUneNote();
    cout << "Saisir la note minimale selon Ernest :" << endl;
    potentielNoteMin = saisirUneNote();
    cout << "Saisir la note maximale selon Ernest :" << endl;
    potentielNoteMax = saisirUneNote();
    cout << "Saisir la moyenne selon Ernest :" << endl;
    potentielMoyenne = saisirUneNote();
    cout << "Saisir les notes des autres eleves : " << endl;
    saisirLesNote();
}

double findNoteMinimale(double t[],int tailleTab)/*Fonction qui cherche et trouve la note minimale*/
{
    double minimale = t[0];
    for(int i=0;i<tailleTab;i++)
    {
        if(t[i]<minimale)
            minimale=t[i];
    }
    return minimale;
}


double findNoteMaximal(double t[],int tailleTab)/*Fonction qui cherche et trouve la note maximale*/
{
    double maximal = 0;
    for(int i=0;i<tailleTab;i++)
    {
        if(t[i]>maximal)
            maximal=t[i];
    }
    return maximal;
}

double calcMoyenne(double t[],int tailleTab)/*Fonction qui calcule la moyenne*/
{
    double total = 0;
    int i=0;
    for(i=0;i<tailleTab;i++)
    {
        total = total+noteEleve[i];

    }
    total = total/i;
    return total;
}

bool verifNote(double t[],int tailleTab,double comparaison){/*Fonction qui vérifie si comparaison et bien dans le tableau t de la taille : taileTab */
    bool note = false;
    for(int i=0;i<tailleTab;i++)                                                 //wheel
    {
        if(verificationNombre(t[i],comparaison))
            note = true;
    }
    return note;
}





int main()/*Point de départ du programme */
{
    saisirNote();


    /*recherche des notes minimale, maximale et calcule la moyenne*/

    double noteMax = findNoteMaximal(noteEleve,nbEleve);

    double noteMin = findNoteMinimale(noteEleve,nbEleve);

    double moyenne = calcMoyenne(noteEleve,nbEleve);


    /*Affiche la note minimale, maximale et la moyenne*/

    cout << "Note minimale : " << noteMin << endl;
    cout << "Note maximal : " << noteMax << endl;
    cout << "Moyenne : " << moyenne <<endl;



    /*Vérifie si les notes saisi par Ernest sont vrai*/

    bool coherenceNoteErnest = verifNote(noteEleve,nbEleve,noteErnest);

    bool coherenceMoyenne = verificationNombre(moyenne,potentielMoyenne);

    bool verifMax = verificationNombre(noteMax,potentielNoteMax);

    bool verifMin = verificationNombre(noteMin,potentielNoteMin);


    /*Affiche le résultat des tests*/
    if(!verifMax)
        cout << "La note maximal est incohérente" << endl;

    if(!verifMin)
        cout << "La note minimale est incohérente" << endl;

    if(!coherenceMoyenne)
        cout << "La moyenne est incohérente" << endl;

    if(!coherenceNoteErnest)
        cout << "La note d'Ernest est incohérente" << endl;

    if(coherenceNoteErnest && coherenceMoyenne && verifMax){
        cout << "Les informations données par Ernest sont cohérentes.(Ernest n'est pas un filou)" << endl;
    }else{
        cout << "Les informations données par Ernest sont incohérentes.(Ernest est un filou)" << endl;
        //cout << "Les informations données par Ernest sont schtroumpfé.(Ernest est un chenapan)" << endl;
    }

    return 0;
}

